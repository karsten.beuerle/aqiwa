export interface Contact {
    id: string;
    index: number;
    guid: string;
    isActive: boolean;
    picture: string;
    age: number;
    gender: string;
    name: string;
    company: string;
    email: string;
    phone: string;
    address: string;
    about: string;
    registered: string;
    latitude: number;
    longitude: number;
    tags: string[];
}
