import { AbstractControl, AsyncValidatorFn, ValidationErrors } from '@angular/forms';
import { map, Observable } from 'rxjs';
import { ContactService } from './_services/contact.service';

export function UniqeEmailValidator(contactService: ContactService): AsyncValidatorFn {
    return (control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> =>
        contactService.findByEmail(control.value)
            .pipe(map(res => (res.length > 0 ? { uniqueEmail: true } : null)));
}

