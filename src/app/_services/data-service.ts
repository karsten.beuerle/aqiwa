import { Observable } from 'rxjs';
/**
 * Defines the base functionality for a data service.
 */
export interface DataService<T> {
    add(entity: T): Observable<T>;
    delete(id: number | string): Observable<number | string>;
    getAll(): Observable<T[]>;
    getById(id: any): Observable<T>;
    update(update: T): Observable<T>;

}
