import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, catchError, EMPTY, map, Observable, of, take, tap } from 'rxjs';
import { DataService } from './data-service';

export class BaseDataService<T> implements DataService<T> {

    allItems$ = new BehaviorSubject<T[]>([]);
    protected url: string;
    private allItemsSource: T[] = [];
    constructor(ressName: string, protected apiUrl: string, protected client: HttpClient) {
        this.url = `${apiUrl}${ressName}`;
        this.getAll()
        .pipe(take(1))
        .subscribe();
    }
    add(entity: T): Observable<T> {
        return this.client.post<T>(this.url, entity)
            .pipe(
                catchError(() => EMPTY),
                tap(item => {
                    this.allItemsSource = [item, ...this.allItemsSource];
                    this.allItems$.next(this.allItemsSource);
                })
            );
    }
    delete<U extends number | string>(id: U): Observable<U> {
        return this.client.delete(`${this.url}/${String(id)}`)
            .pipe(
                catchError(() => EMPTY),
                map(() => id),
                tap(itemId => {
                    this.allItemsSource = [...this.allItemsSource.filter(i => i['id'] !== itemId)];
                    this.allItems$.next(this.allItemsSource);
                })
            );
    }
    getAll(): Observable<T[]> {
        return this.client.get<T[]>(this.url)
            .pipe(catchError(() =>
                of([] as T[])),
                tap(items => {
                    this.allItemsSource = [...items];
                    this.allItems$.next(this.allItemsSource);
                })
            );
    }
    getById(id: string | number): Observable<T> {
        return this.client.get<T>(`${this.url}/${id}`);
    }
    update(update: T): Observable<T> {
        throw new Error('Method not implemented.');
    }
}
