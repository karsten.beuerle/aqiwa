import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Contact } from '../contact';
import { API_BASE_URL } from '../tokens';
import { BaseDataService } from './base-data-service';

@Injectable({
  providedIn: 'root'
})
export class ContactService extends BaseDataService<Contact> {

  constructor(@Inject(API_BASE_URL) protected apiUrl: string, protected client: HttpClient) {

    super('Contacts', apiUrl, client);
  }

  findByEmail(email: string): Observable<Contact[]> {
    return this.client.get<Contact[]>(`${this.url}?email=${email}`);
  }

}
