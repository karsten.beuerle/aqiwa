import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { lastValueFrom } from 'rxjs';
import { UniqeEmailValidator } from '../uniqe-email-validator';
import { ContactService } from '../_services/contact.service';

@Component({
  selector: 'app-new-contact',
  templateUrl: './new-contact.component.html',
  styleUrls: ['./new-contact.component.less']
})
export class NewContactComponent implements OnInit {
  contactForm: FormGroup;

  constructor(private readonly contactService: ContactService, private router: Router) { }

  get name(): AbstractControl {
    return this.contactForm?.get('name');
  }

  get age(): AbstractControl {
    return this.contactForm.get('age');
  }
  get email(): AbstractControl {
    return this.contactForm.get('email');
  }
  get phone(): AbstractControl {
    return this.contactForm.get('phone');
  }
  ngOnInit(): void {
    this.contactForm = new FormGroup({
      name: new FormControl(undefined, Validators.required),
      age: new FormControl(undefined, [Validators.required, Validators.min(18)]),
      email: new FormControl(undefined, [Validators.required, Validators.email], UniqeEmailValidator(this.contactService)),
      // TODO add pattern validation, if needed
      phone: new FormControl(undefined, [Validators.required])
    });
  }

  save(): void {
    if (this.contactForm.valid) {
      const contact = this.contactForm.getRawValue();
      contact.registered = new Date();
      lastValueFrom(this.contactService.add(contact))
        .then(res => {
          // tslint:disable-next-line: no-floating-promises
          this.router.navigate(['contact', 'detail', res.id]);
        },
          // tslint:disable-next-line: no-empty
          () => { });
    }

  }
}


