import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter, map, Observable, of, Subject, switchMap, takeUntil, withLatestFrom } from 'rxjs';
import { Contact } from '../contact';
import { Destroyable } from '../destroyable';
import { ContactService } from '../_services/contact.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.less']
})
export class ContactComponent extends Destroyable implements OnInit {
  contacts$: Observable<Contact[]>;
  selectedContactId: number;

  constructor(private readonly contactService: ContactService, route: ActivatedRoute, router: Router) {
    super();
    router.events.pipe(
      takeUntil(this.destroy$),
      filter(e => e instanceof NavigationEnd),
      switchMap(() => {
        if (!!route.firstChild) {
          return route.firstChild.params.pipe(map(p => +p.contactId));
        // an 'else' seems to be more readable here
        // tslint:disable-next-line: unnecessary-else
        } else {
          return of(-1);
        }
      })
    )
      .subscribe(contactId => {
        this.selectedContactId = contactId;
      });

  }

  ngOnInit(): void {
    this.contacts$ = this.contactService.allItems$;
  }

}
