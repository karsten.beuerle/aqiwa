import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewContactComponent } from '../new-contact/new-contact.component';
import { ContactDetailComponent } from './contact-detail/contact-detail.component';
import { ContactComponent } from './contact.component';

const routes: Routes = [{
  path: '', component: ContactComponent,
  children: [
    { path: 'detail/:contactId', component: ContactDetailComponent },
    { path: 'create', component: NewContactComponent }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContactRoutingModule { }
