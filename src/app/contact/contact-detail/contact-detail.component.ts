import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { lastValueFrom, Observable, switchMap } from 'rxjs';
import { Contact } from '../../contact';
import { ContactService } from '../../_services/contact.service';

@Component({
  selector: 'app-contact-detail',
  templateUrl: './contact-detail.component.html',
  styleUrls: ['./contact-detail.component.less']
})
export class ContactDetailComponent implements OnInit {
  contact$: Observable<Contact>;

  constructor(private readonly contactService: ContactService, route: ActivatedRoute, private readonly router: Router) {
    // load the contact details as soon as an contact id is available in route params
    this.contact$ = route.params.pipe(
      switchMap(p => contactService.getById(p.contactId)));
  }

  // tslint:disable-next-line: no-empty
  ngOnInit(): void {}

  delete(id: string): void {
    // tslint:disable-next-line: no-floating-promises
    lastValueFrom(this.contactService.delete(id))
    .then(() =>
      this.router.navigate(['contact']));
  }

}
