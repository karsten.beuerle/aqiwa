import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Pipe({
  name: 'active'
})
export class ActivePipe implements PipeTransform {

  constructor(private readonly sanitizer: DomSanitizer) { }
  transform(value: boolean, ...args: any[]): SafeHtml {
    // active: green (class: text-success), inactive: red (class: text-danger)
    const htmlString = !!value ? '<i class="fas fa-circle text-success"></i>' : '<i class="fas fa-circle text-danger"></i>';

    return this.sanitizer.bypassSecurityTrustHtml(htmlString);
  }

}
