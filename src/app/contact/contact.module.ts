import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MdbFormsModule } from 'mdb-angular-ui-kit/forms';

import { NewContactComponent } from '../new-contact/new-contact.component';
import { GenderPipe } from '../_pipes/gender.pipe';
import { ActivePipe } from './active.pipe';
import { ContactDetailComponent } from './contact-detail/contact-detail.component';
import { ContactRoutingModule } from './contact-routing.module';
import { ContactComponent } from './contact.component';


@NgModule({
  declarations: [
    ContactComponent,
    GenderPipe,
    ContactDetailComponent,
    ActivePipe,
    NewContactComponent
  ],
  imports: [
    CommonModule,
    ContactRoutingModule,
    ReactiveFormsModule,
    MdbFormsModule

  ]
})
export class ContactModule { }
