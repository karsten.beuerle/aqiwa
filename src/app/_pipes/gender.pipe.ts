import { Pipe, PipeTransform } from '@angular/core';

/**
 * Pipe used to represent the gender of a person as an emoji.
 *
 * @implements {PipeTransform}
 */
@Pipe({
  name: 'gender'
})
export class GenderPipe implements PipeTransform {

/**
 * Returns the appropriate emoji.
 *
 * @param {string} value
 * @param {...any[]} args
 * @return {*}  {string}
 */
transform(value: string, ...args: any[]): string {
    switch (value) {
      case 'female': {
        return '👩';
      }
      case 'male': {
        return '👨';
      }
      default: {
        return '😀';
      }
    }

  }

}
